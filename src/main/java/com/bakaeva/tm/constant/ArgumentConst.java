package com.bakaeva.tm.constant;

public interface ArgumentConst {

    String HELP = "-h";

    String VERSION = "-v";

    String ABOUT = "-a";

    String INFO = "-i";

    String ARGUMENTS = "-arg";

    String COMMANDS = "-c";

}
