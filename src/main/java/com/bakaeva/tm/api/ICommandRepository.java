package com.bakaeva.tm.api;

import com.bakaeva.tm.model.TerminalCommand;

import java.util.Arrays;

public interface ICommandRepository {
    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    public TerminalCommand[] getTerminalCommands();

    public String[] getCommands();

    public String[] getArgs();

}
